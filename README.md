# Infrastructure

This repo holds all the infrastructure related tools for the deployment/maintenance of hrc.
Each tool is a submodule for a dedicated repository.

```
ansible   -> All ansible playbooks, modules, etc are here
docker    -> All Docker related Dockerfiles and Docker Compose
isos      -> Custom ISOs builder
docs      -> Public documentation
```

## Branches

* main

The main branch is the top level branch that should, in most circumstances, not take direct changes.

Each new feature should have is own branch
