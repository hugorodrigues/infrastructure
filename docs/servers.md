# Servers

## Purchases

### NUC

| Component | Model                    | SKU           | Price  | Quanity | Total  |
|-----------|--------------------------|---------------|--------|---------|--------|
| NUC       | Intel NUC NUC7CJYHN      | BOXNUC7CJYHN2 | 139.90 | 1       | 139.90 |
| RAM       | Crucial DDR4 4GB 2400Mhz | CT4G4SFS824A  | 20.90  | 2       | 41.80  |

Grand total: 181.70

### Whitebox Builtin

| Component | Model                    | SKU              | Price  | Quanity | Total  |
|-----------|--------------------------|------------------|--------|---------|--------|
| Board     | Asrock J4125B-ITX        | 90-MXBCH0-A0UAYZ | 115.90 | 1       | 115.90 |
| RAM       | Crucial DDR4 4GB 2400Mhz | CT4G4SFS824A     | 20.90  | 2       | 41.80  |
| PSU       | Seasonic Core GM 500W    | CORE-GM-500      | 69.69  | 1       | 69.90  |
| Case      | TBD                      | TBD              | TBD    | TBD     | TBD

Grand total: 227.6

### Whitebox Celeron


| Component | Model                    | SKU              | Price  | Quanity | Total  |
|-----------|--------------------------|------------------|--------|---------|--------|
| Board     | 
| CPU       | Intel Celeron G5905      | BX80701G5905     | 44.90  | 1       | 44.90  |
| RAM       | 
